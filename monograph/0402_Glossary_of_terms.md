## 수식의 변수, 연산자, 함수 및 상수
*(a.u.: arbitrary units; e.g., metres (m), radians (rad), etc)*

| 표현식                                                   | 설명                                                                   | 단위              |
|---------------------------------------------------------|:----------------------------------------------------------------------|-------------------------|
|                                                         | **변수**                                                              |                      |
| $\tau$                                                  | 시간                                                                   | s(seconds)              |
| $x[\tau]=\{x(t):t\in(0,\tau)\}$                         | 상태공간에서 궤적이나 패스                                                  | a.u. (m)                |
| $\omega(\tau)$                                          | 무작위 요동                                                              | a.u. (m)                |
| $\begin{aligned}x^{(i)}&=\{x_1^{(i)},x_2^{(i)},\ldots,x_N^{(i)}\} \\x_n^{(i)}&=\{x_{n_1}^{(i)},x_{n_2}^{(i)},\ldots,x_{n_M}^{(i)}\} \end{aligned}$| $i$ 번째 설명 수준의 벡터 상태 | a.u. (m) |
| $x=\{\eta,s,a,\mu\}\in X$                               | 마르코프 분할은 외부, 감각, 활동 및 내부 상태로 나눔                             | a.u. (m)                |
| $\tilde\eta\in X\backslash E:x=\{\eta,\tilde\eta\}\in X$| 상태 부분 집합의 여집합                                                     | a.u. (m)                |
| $\dot x=\frac{dx}{dt}$                                  | 시간 미분(뉴턴 표기법)                                                     | m/s                     |
| $\vec x=(x,x^{'},x^{''},\ldots)$                        | 일반화된 운동(라그랑지안 표기법)                                              | (m,m/s,...)             |
| $\alpha=\{a,\mu\}\in A$                                 | 자율 상태                                                               | a.u. (m)                |
| $b=\{s,a\}\in B$                                        | 담요 상태                                                               | a.u. (m)                |
| $\pi=\{b,\mu\}\in P$                                    | 특정 상태                                                               | a.u. (m)                |
| $\eta\in E$                                             | 외부 상태                                                               | a.u. (m)                |
| $\Gamma=\tfrac h{2m}=\mu_m k_B T$                       | 무작위 요동의 진폭 (즉, 분산의 절반)                                          | $m^2$/s or J$\cdot$s/kg |
| $\mathcal Q$                                            | 솔레노이드 흐름의 정도                                                      | $m^2$/s or J$\cdot$s/kg |
| $m=\tfrac{\hbar}{2\mu_m k_b T}=\tfrac{\hbar}{2\Gamma}$  | (감소된) 질량                                                            | kg(kilogram)            |
| $\mu_m=\tfrac{\hbar}{2mk_B T}=\tfrac{1}{k_B T}\Gamma$   | 이동성 계수                                                              | s/kg                    |
| $T$                                                     | 온도                                                                   | K(Kelvin)               |
| $\ell=\int d\ell :d\ell^2=g_{ij}d\lambda^j d\lambda^i$  | 정보 길이                                                               | nats                    |
| $D(\tau)=D[p(x,\tau\vert\pi_0)\parallel p(x,\infty\vert pi_0)]$ | 발산 길이                                                       | nats                    |
| $\pmb\tau=d\ell(\tau\geq\pmb\tau)\approx 0$             | 임계 시간                                                               | s                       |
| $g_{ij}=E\lbrack\frac{\partial\Im}{\partial\lambda^i}\frac{\partial\Im}{\partial\lambda^i}\rbrack$| 피셔(정보 메트릭)텐서            | a.u.                   |
|                                                         | **함수, 함수적 표현 그리고 퍼텐셜**                                        |                        |
| $E[x]=[E_p[x]=\int xp_\lambda(x)dx$                     | 기대치 또는 평균                                                          |                        |
| $p_\lambda(x):Pr[X\in A]=\int_A p_\lambda(x)dx$         | 충분한 통계에 의해 매개변수화된 확율 밀도 함수 $\lambda$                         |                       |
| $p_\tau(x)\equiv p(x,\tau)$                             | 시간에 의해 매개변수화된 시간의존 확율 밀도 함수                                  |                       |
| $\begin{aligned}p(x)&\triangleq p_\infty(x)=\lim_{\tau\to\infty}(p(x,\tau)\\ &\Leftrightarrow \pmb L p(x)= 0\end{aligned}$ | 비평형 안정상태 밀도 - 포커-프랑크 연산자의 고유해 ||
| $q_mu(\eta)$                                            | 변분 밀도 - 내부상태의 매개변수화된 외부 상태의 (거의 후반) 밀도                     |                      |
| $f(x)=E[\dot x]$                                        | 흐름 - 상태공간에서 기대 운동                                                |                        |
| $j(x)=f(x)p(x)-\Gamma\nabla p(x)$                       | 확율 흐름                                                               |                         |
| $\begin{aligned}\Psi(x,t)&=\Psi(x)e^{-i\omega t}\\p(x)&=\Psi(x)\cdot\Psi(x)^\dagger\end{aligned}$ | 파동 함수 - 비평형 안정상태 밀도의 복소근 |                   |
| $\mathcal L(x,\dot x)=\tfrac 1 2[(\dot x-f)\cdot\tfrac 1 {2\Gamma}(\dot x-f)+\nabla\cdot f]$ | 라그랑지안 (해밀토니안의 르장드르 변환)       ||
| $\mathcal H(x,\dot x)=\dot x\cdot \tfrac{\partial\mathcal L}{\partial\dot x}-\mathcal L(x,\dot x)$ | 해밀토니안 (라그랑지안의 르장드르 변환) ||
| $\mathcal A(x[\tau])=\Im(x[\tau])=\int_0^t\mathcal(x,\dot x)d\tau$ | 행위: 경로의 놀라움; 예: 라그랑지안의 경로 적분                    |                         |
| $V(x)=\tfrac m 2 f\cdot f+\tfrac \hbar 2 \nabla\cdot f$ | 슈뢰딩거 퍼텐셜                                                          | J(Joules)               |
| $U(\pi)=k_B T\cdot\Im(\pi)+F_m$                         | 열역학적 퍼텐셜                                                          | J or kg $m^2$/$s^2$     |
| $F_\Im(\tau)\triangleq E[U(\pi,\tau)]-E[k_B T\cdot\Im(\pi)]$ | 열역학적 자유 에너지                                                 | J or kg $m^2$/$s^2$     |
| $F(\pi)\geq\Im(\pi)$                                    | 변분 자유 에너지 - 특정 상태의 놀라움 상한                                    | nats                    |
| $G(\alpha_\tau)\geq\Im_\tau(\alpha_\tau\vert\pi_0))$    | 기대 자유 에너지 - 미래의 자율 상태 놀라움의 상한                               | nats |
| $\Omega(x[\tau])$                                       | 경로-의존 측정                                                           | a.u.                    |
| $\pmb\sigma(u_i)=\frac{exp(-u_i)}{\sum_i exp(-u_i)}$    | 벡터의 정규화된 지수 함수(softmax)                                         | a.u.                    |
|                                                         |**연산자**                                                              |                         |
| $u\cdot v= u^T v = \braket{u\vert v}=u_i v^i$           | 디렉 합산 표기법을 사용한 내적                                              |                         |
| $u\times v=uv^T = \ket{u}\bra{v}$                       | 크로스 디렉 표기법을 사용한 외적                                             |                         |
| $\nabla_x\Im(x)=\frac{\partial\Im}{\partial x}=\big( \frac{\partial\Im}{\partial x_1}, \frac{\partial\Im}{\partial x_2},\cdots\big)$ | 차분 또는 기울기 연산자 (스칼라 장에서)||
| $\nabla_{xx}\Im(x)=\frac{\partial^2\Im}{\partial x^2}$  | 곡면 연산자(스칼라 장에서)                                                 |                         |
| $\nabla\cdot f(x) = \sum_i\frac{\partial f_i}{\partial x_i}$ | 발산 연산자 (벡터장에서)                                              |                         |
| $\nabla^2\Im(x)=\nabla\cdot\nabla\Im=\Delta\Im$         | 라프라스 연산자 - 기울기의 발산                                             |                         |
| $\begin{aligned}\pmb L&=\nabla\cdot(\Gamma\nabla-f) \\ \dot x&=\pmb L p(x)\end{aligned}$ | 포커-프랭크 연산자                       |                         |
| $\begin{aligned}\pmb H&=V(x)-\tfrac{\hbar^2}{2m}\nabla^2 \\ i\hbar\dot\Psi(x)&=\pmb H\Psi(x)\end{aligned}$ | 해밀토니안 연산자     |                       |
| $\pmb T = -\tfrac{\hbar^2}{2m}\nabla^2$                 | 운동 연산자(Kinetic operator)                                           |                         |
| $\Im(x)=-\ln p(x)$                                      | 놀라움 또는 자기 정보                                                    | nats                    |
| $H(X)=H[p(x)]=E[\Im(x)]$                                | 엔트로피 또는 기대된 놀라움                                                | nats                    |
| $H(X\vert Y) = E_{p(x,y)}[\Im(x\vert y)]$               | 조건 엔트로피                                                           | nats                    |
| $D[q(x)\parallel p(x)]=E_q[\ln q(x)-\ln p(x)]$          | 상대적 엔트로피 또는 쿨백-라이블러 발산(Kullback-Leibler divergence)         | nats                    |
| $\begin{aligned}I(X,Y)&=H(X)-H(X\vert Y)\\&=H(X)+H(Y)-H(X,Y)\\&=D[p(x,y)\parallel p(x)p(y)]\geq0\end{aligned}$ | 상호 정보량      | nats or natural units 1 nat $\approx$ 1.44 bits |
| $S\triangleq k_B H(X)$                                  | 열역학적 엔트로피                                                         | J/K or $m^2$kg $s^{-2}$ /K |
|                                                         | **상수 및 계수**                                                        |                   |
| $Z$                                                     | 분할 함수 또는 정규화 상수                                                 | a.u                     |
| $\hbar=\tfrac \hbar {2\pi}$                             | (축소된)플랭크 상수(디렉 상수) $1.05457\times 10^{-34}$                    | $m^2$ kg/s or J$\cdot$ s|
| $k_B$                                                   | 볼츠만 상수 $1.39\times 10^{-23}$                                       | $m^2$ kg $s^{-2}$/K or J/K |
| c                                                       | 광속 299,792,458                                                       | m/s                     |

[TABLE 6 용어정의](./img/t6.png)